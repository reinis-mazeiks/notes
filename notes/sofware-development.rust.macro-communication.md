---
id: wsxlo05a1jpmb2dtqt0j8i0
title: Macro Communication
desc: ""
updated: 1683990827771
created: 1683987791738
---

# Type System

Rust has a pretty powerful type system. Sometimes it can help avoid macros altogether.

// todo

# Global Variables 🤮

Static variables (say `Lazy<Mutex<_>>`) will persist accross macro invocations in the same crate. You can use this to pass data around.

For example, see [code for enum_dispatch](https://gitlab.com/antonok/enum_dispatch/-/blob/master/src/cache.rs)

Drawbacks:

- Macros invoked first can send data to macros invoked later, but not the other way around. In practice, this forced `enum_dispatch` to silently ignore some errors (see docs for more details)
- Cannot communicate accross macro invocations in different crates

# Namespace Hack

Rust items live in 3 different [namespaces](https://doc.rust-lang.org/reference/names/namespaces.html) - types, value and macro.

This means you can, for example, declare a constant and a struct with the same name:

```rust
const Foo: i32 = 0;

struct Foo {}
```

Importantly, any path to `Foo`, be it an absolute path or something imported with a `use` statement, can be interpreted as both the value and the constant.

This allows one macro to attatch "metadata" in the form of a constant. Then, if another macro is given a path to `Foo`, it can emmit code that reads the associated metadata, effectively passing data from one macro to another. Naturally, this will work even if the macros are invoked in different crates.

Drawbacks:

- You cannot have multiple constants with the same name - so if 2 macros try to use this technique on the same item, it won't compile.
- Unit structs such as `struct Foo;` use both the type and the value namespace.
- The value of the constant cannot be read at compile time, as macros operate on syntax only

# Namespace hack + macro_rules

Macros can emmit macros. If we abuse this together with the namespace hack, we get something pretty powerful.

As an example, consider how my crate [enum_delegate](https://crates.io/crates/enum_delegate) used (!) to work.

We have 2 macros - `register`, which needs to be attached to a trait, and `implement`, which will implement a specified registered trait on the enum it is attached to.

Most generally, to generate an implementation of trait T for an enum E like we do, we need 4 things:

- (1) The path to trait T (since it might not be in scope)
- (2) The definition of trait T (to know what methods we need to delegate, etc.)
- (3) The path to enum E
- (4) The definition of enum E (to know what the variant names & types are)

The `implement` macro requires the user to pass in the path to the trait to implement (1). Since it annotates the enum, it can also see the enum definition (4) and, since the code generation happens at the call site, it can infer the path to the enum (3).

The problem is that `implement` can't see the trait definition (2). The solution is to annotate the trait with a `register` macro, and pass this information along to the `implement` macro.

The trick is to generate a macro right next to the trait. We can generate a new `macro_rules` macro that "knows" the trait definition, and generates the necessary code when given the trait path, enum path and enum declaration.

To make it available in other crates, we must `macro_export` it. This will make it available at top-level, so it needs a unique name to not clash with other traits of the same name.

```rust, ignore
    quote! {
        #[macro_export]
        macro_rules! #macro_name {
            ($trait_path: path, $enum_path: path, $enum_declaration: item) => {
                enum_delegate::implement_trait_for_enum!{
                    $trait_path,
                    #parsed_trait,
                    $enum_path,
                    $enum_declaration
                }
            };
        } // ...
    }
```

> Note: `enum_delegate::implement_trait_for_enum` is a function macro that generates the actual implementation. It is not part of the public API.

To allow the `implement` macro to "find" this generated macro, we export it with the same name as the trait - which is possible since macros and traits live in different namespaces

```rust, ignore
pub use #macro_name as #trait_name;
```

All the `implement` macro has to do now is call this newly generated macro with the path to the trait (provided by the user), the enum path and declaration (parsed from the annotated enum).

So yeah. We define a macro that generates another macro used by a third macro.

Drawbacks:

- Compile times might be affected
- May be hard to debug
