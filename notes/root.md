---
id: sut2gtu7755tvu1qd8fo2kv
title: Root
desc: ""
updated: 1683990819107
created: 1681213997245
---

Some notes, mostly related to development.

Kind of like a blog, but less polished, and maybe occasionally updated with new info.

[Source on GitLab](https://gitlab.com/reinis-mazeiks/notes). PRs for typos, inaccuracies, etc. always welcome.
